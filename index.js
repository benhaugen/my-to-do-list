const userEndPoint = "http://localhost:3000/api/v1/users"
const taskEndPoint = "http://localhost:3000/api/v1/tasks"
const form = document.getElementById('taskForm');
const input = document.getElementById('taskInput');
const taskList = document.getElementById('taskList')
const addedTasks = document.getElementById('addedTasks')
// let list = [];
// let editing = false;
let description = ''

document.addEventListener('DOMContentLoaded', () => {
  getLoginInfo(), getName(), todos()
})


function getLoginInfo() {
  let login = document.getElementById('login')
  login.addEventListener('submit', (ev) => {
    ev.preventDefault()
    let name = document.getElementById('name').value
    let password = document.getElementById('password').value
    let data = {
      'name': name,
      'password': password
    }
    postUser(data)
  })
}

function getName() {
  let nameDiv = document.getElementById('printName')
  let login = document.getElementById('login')
  login.addEventListener('submit', (ev) => {
  let name = document.getElementById('name').value
  localStorage.setItem('nameData', name)
  let data = localStorage.getItem('nameData')
  upperCaseName = data.charAt(0).toUpperCase() + data.substring(1);
  nameDiv.innerText += `Hello there ${upperCaseName} ☺️`
  })
}

function postUser(data) {
    fetch(userEndPoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(json => {
      localStorage.setItem('userId', json.id)
      retrieveUsersTasks()
    })

    toggleDiv('outerBox')
    toggleDiv('login')
    let loginDiv = document.getElementById('login')
    let form = document.getElementById('loginForm')
    form.style.display = "none"
  }

async function todos() {
  form.addEventListener("submit", async function(event) {
    event.preventDefault()
    let newTask = input.value
    let listItem = document.createElement('li')
    let deleteButton = document.createElement("button")
    deleteButton.innerText = "Delete"
    deleteButton.id = 'delete'
    listItem.setAttribute('contenteditable', true)
    deleteButton.setAttribute('contenteditable', false)
    listItem.innerText = newTask
    listItem.appendChild(deleteButton)
    addedTasks.appendChild(listItem)
    taskList.appendChild(addedTasks)
    description = input.value
    const task = await fetchPostTaskInfo()
    form.reset()

    function deleteTask() {
  		let listItem=this.parentNode;
  		let ul=listItem.parentNode;
  		ul.removeChild(listItem);
  }
    deleteButton.onclick=deleteTask
    deleteButton.addEventListener('click', () => {
      deleteTaskFromDataBase(task.id)
    })
  })
}


function retrieveUsersTasks() {
  fetch(userEndPoint + '/' + localStorage.userId)
  .then(res => res.json())
  .then(user => {
    showUsersTasks(user)
  })
}

function showUsersTasks(user) {
  for (const task of user.tasks) {
    let li = document.createElement('li')
    li.textContent = task.description
    addedTasks.appendChild(li)
    let deleteButton = document.createElement("button")
    // let editButton = document.createElement('button')
    // editButton.innerText="Edit";
  	// editButton.id="editButton";
    deleteButton.innerText = "Delete"
    deleteButton.id = 'delete'
    li.setAttribute('contenteditable', true)
    // editButton.setAttribute('contenteditable', false)
    deleteButton.setAttribute('contenteditable', false)
    // li.appendChild(editButton);
    li.appendChild(deleteButton)

    function deleteSavedTask() {
  		let listItem=this.parentNode;
  		let ul=listItem.parentNode;
  		ul.removeChild(listItem);
  }
    deleteButton.onclick=deleteSavedTask
    deleteButton.addEventListener('click', () => {
      deleteTaskFromDataBase(task.id)
    })
  }
}

async function fetchPostTaskInfo() {
    let data = {
      'description': description,
      'user_id': localStorage.getItem('userId')
    }
    const result = await fetch(taskEndPoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(data)
    })
    const json = await result.json()
    return json
}

function deleteTaskFromDataBase(id) {
  let data = {
    'id': id
  }
  fetch(taskEndPoint + `/${id}`, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json', Accept: 'application/json'},
    body: JSON.stringify(data)
  })
}

// function editTask() {
//   let editButton = document.getElementById('editButton')
//   editButton.addEventListener('click', () => {
//     alert("LOL! You can't edit a task fool! Just delete it and make a new one lazy")
  //   var listItem=this.parentNode;
  //   var editInput=listItem.querySelector('input[type=text]');
  //   var label=listItem.querySelector("label");
  //   var containsClass=listItem.classList.contains("editMode");
  //     if(containsClass){
  //
  //       //switch to .editmode
  //       //label becomes the inputs value.
  //         label.innerText=editInput.value;
  //       }else{
  //         editInput.value=label.innerText;
  //       }
  // })
  // listItem.classList.toggle("editMode");
// }

function toggleDiv(id) {
  let toggled = document.getElementById(id);
  if (toggled.style.display === "block") {
    toggled.style.display = "none";
  } else {
    toggled.style.display = "block";
  }
}


// form.addEventListener("submit", function(event) {
//   event.preventDefault()
//   let newTask = input.value
//   let listItem = document.createElement('li')
//   // let editButton = document.createElement('button')
//   // editButton.innerText="Edit";
// 	// editButton.id="editButton";
//   let deleteButton = document.createElement("button")
//   deleteButton.innerText = "Delete"
//   deleteButton.id = 'delete'
//   listItem.setAttribute('contenteditable', true)
//   // editButton.setAttribute('contenteditable', false)
//   deleteButton.setAttribute('contenteditable', false)
//   listItem.innerText = newTask
//   // let label = document.createElement('label')
//   // var editInput = document.createElement('input')
//   // editInput.type = 'text'
//   // let textHolder = document.createElement("span")
//   // listItem.textContent = newTask
//   // editInput.type="text";
//   // editInput.innerText=newTask;
//   // listItem.appendChild(editInput)
//   // listItem.appendChild(editButton);
//   listItem.appendChild(deleteButton)
//   addedTasks.appendChild(listItem)
//   taskList.appendChild(addedTasks)
//   // editTask();
//   description = input.value
//   fetchPostTaskInfo()
//   form.reset()
//
//   function deleteTask() {
// 		let listItem=this.parentNode;
// 		let ul=listItem.parentNode;
// 		ul.removeChild(listItem);
// }
//   deleteButton.onclick=deleteTask
// })
